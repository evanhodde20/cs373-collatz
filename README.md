# CS373: Software Engineering Collatz Repo

* Name: Evan Hodde

* EID: emh3297

* GitLab ID: evanhodde20

* HackerRank ID: evanhodde20

* Git SHA: 13baa6c98e26b66556ac380f14223dd4557bb1e9

* GitLab Pipelines: https://gitlab.com/evanhodde20/cs373-collatz/pipelines

* Estimated completion time: 12 hours

* Actual completion time: 308:56:25

* Comments: N/A
