#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(957373, 997793)
        self.assertEqual(v, 458)

    def test_eval_5(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_6(self):
        v = collatz_eval(612960, 858107)
        self.assertEqual(v, 525)

    def test_eval_7(self):
        v = collatz_eval(868237, 969543)
        self.assertEqual(v, 507)

    def test_eval_8(self):
        v = collatz_eval(575858, 752817)
        self.assertEqual(v, 509)

    def test_eval_9(self):
        v = collatz_eval(779843, 843148)
        self.assertEqual(v, 525)

    def test_eval_10(self):
        v = collatz_eval(873166, 902188)
        self.assertEqual(v, 445)

    def test_eval_11(self):
        v = collatz_eval(609639, 914884)
        self.assertEqual(v, 525)

    def test_eval_12(self):
        v = collatz_eval(341365, 885819)
        self.assertEqual(v, 525)

    def test_eval_13(self):
        v = collatz_eval(549520, 716000)
        self.assertEqual(v, 509)

    def test_eval_14(self):
        v = collatz_eval(979636, 663)
        self.assertEqual(v, 525)

    def test_eval_15(self):
        v = collatz_eval(892702, 969778)
        self.assertEqual(v, 507)

    def test_eval_16(self):
        v = collatz_eval(255868, 309647)
        self.assertEqual(v, 407)

    def test_eval_17(self):
        v = collatz_eval(823826, 860095)
        self.assertEqual(v, 525)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
